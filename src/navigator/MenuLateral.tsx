import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { DrawerContentComponentProps, DrawerContentScrollView, createDrawerNavigator } from '@react-navigation/drawer';
import { SettingsScreen } from '../screens/SettingsScreen';
import { Image, Text, TouchableOpacity, View, useWindowDimensions } from 'react-native';
import { styles } from '../theme/appTheme';
import { Tabs } from './Tabs';

const Drawer = createDrawerNavigator();

export const MenuLateral = () => {

  const { width } = useWindowDimensions();

  return (
    <Drawer.Navigator
      screenOptions={{
        drawerType: width >= 768 ? 'permanent' : 'front',
      }}
      drawerContent={ (props) => <MenuInterno {...props} /> }
    >
      <Drawer.Screen name="Tabs" component={Tabs} />
      <Drawer.Screen name="SettingsScreen" component={SettingsScreen} />
    </Drawer.Navigator>
  );
};

const MenuInterno = ({ navigation }: DrawerContentComponentProps) => {

  return (
    <DrawerContentScrollView>
      {/* Parte del avatar */}
      <View style={styles.avatarContainer}>
        <Image
          source={{
          uri: 'https://alumni.engineering.utoronto.ca/files/2022/05/Avatar-Placeholder-400x400-1.jpg',
          }}
          style={styles.avatar}
        />
      </View>

      {/* Opciones de menú */}
      <View style={styles.menuContainer}>
        <TouchableOpacity style={{...styles.menuBoton, flexDirection: 'row', alignItems: 'center'}} onPress={() => navigation.navigate('Tabs')}>
          <Icon name="navigate-outline" size={20} color="black" />
          <Text style={styles.menuTexto}> Navegación</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{...styles.menuBoton, flexDirection: 'row', alignItems: 'center'}} onPress={() => navigation.navigate('SettingsScreen')}>
          <Icon name="settings-outline" size={20} color="black" />
          <Text style={{...styles.menuTexto, justifyContent: 'center' }}> Ajustes</Text>
        </TouchableOpacity>
      </View>
    </DrawerContentScrollView>
  );
};
