import React, { useEffect } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

import { Text, View } from 'react-native';
import { colores, styles } from '../theme/appTheme';

export const Tab1Screen = () => {

  useEffect(() => {
    console.log('Tab1Screen effect');
  }, []);


  return (
    <View style={ styles.globalMargin }>
      <Text style={ styles.title }>Iconos</Text>

      <Text>
        <Icon name="airplane-outline" size={80} color={ colores.primary } />
        <Icon name="accessibility-outline" size={80} color={ colores.primary } />
        <Icon name="battery-full-outline" size={80} color={ colores.primary } />
        <Icon name="basketball-outline" size={80} color={ colores.primary } />
        <Icon name="camera-outline" size={80} color={ colores.primary } />
        <Icon name="code-slash-outline" size={80} color={ colores.primary } />
      </Text>
    </View>
  );
};
